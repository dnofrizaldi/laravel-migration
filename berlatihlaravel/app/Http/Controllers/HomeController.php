<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama()
    {
        return view('welcome');
    }

    public function account()
    {
        return view('page.account');
    }

    public function home(request $request)
    {
        dd($request);
        // $nama = $request['name'];
        // $namalast = $request['name'];

        // return view('page.home', ['name' => $nama, 'name' => $namalast]);
    }
}
